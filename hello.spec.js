const hello = require('./hello');

describe('test hello function', () => {
  test('hello should return "hello world"', () => {
    expect(hello()).toBe('hello world');
  });
});